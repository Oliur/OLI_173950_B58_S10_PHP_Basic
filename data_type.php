<?php

    // 8 data types in php
        // 4 data types : Scalar
            //1. Boolean
                $myTestVar = true;
                var_dump($myTestVar);
                     echo "<br>";
            //2. Integer
                $myTestVar = 85;
                var_dump($myTestVar);
                    echo "<br>";
            //1. Float
                $myTestVar = 80.599;
                var_dump($myTestVar);
                    echo "<br>";
            //2. String
                $myTestVar = "OLI";
                var_dump($myTestVar);
                    echo "<br>";
        // 2 data type : compound
            //1. Array
                //1.indexed array
                    $person = array(
                        array("x",4,4.8),
                        array("Y",8,4.6),
                        array("Z",16,5.8)
                    );

                //2. Associative array
                    echo "<br>";
                    $age = array(
                        "Kuddus" => 45,
                        "Jorina" => 30,
                        "Moynar Ma" => 45,
                        "Abul" => 66
                    );
                    echo $age ["Jorina"];

                    echo "<br>";
            //2. Object

                    echo "<br>";

        //2 data type : Special
            //1.Resource
                    echo "<br>";
            //2.NULL